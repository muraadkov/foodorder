﻿using FoodOrder.Interfaces;
using FoodOrder.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodOrder.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly FoodContext context;
        public UserRepository(FoodContext context)
        {
            this.context = context;
        }
        public IEnumerable<User> GetAll()
        {
            return context.User;
        }

        public List<User> GetUserById(int userId)
        {
            var user = context.User.Where(user => user.UserId == userId);
            return user.ToList();
        }

        public List<User> GetUserByUsername(string username)
        {
            var user = context.User.Where(user => user.Username == username);
            return user.ToList();
        }

        public List<User> GetUserByFname(string fname)
        {
            var user = context.User.Where(user => user.Fname == fname);
            return user.ToList();
        }

        public List<User> GetUserByLname(string lname)
        {
            var user = context.User.Where(user => user.Lname == lname);
            return user.ToList();
        }

        public List<User> GetUserByEmail(string email)
        {
            var user = context.User.Where(user => user.Email == email);
            return user.ToList();
        }

        public IEnumerable<User> Get(int userId)
        {
            return context.User;
        }

        public void CreateUser(User item)
        {
            context.User.Add(item);
            context.SaveChanges();
        }

        public void UpdateUser(User item)
        {
            context.User.Update(item);
            context.SaveChanges();
        }

        public User DeleteUser(int userId)
        {
            var item = context.User.Find(userId);

            if (item != null)
            {
                context.User.Remove(item);
                context.SaveChanges();
            }
            return item;
        }
    }
}
